module.exports =

'Basic Checkups': (test) ->
  test
    .open('http://localhost:3000')
    .assert.chain()
      .title().is('jsplumbbacktest', ': correct')
      # .visible("body", ": body")
      .exists("#selectNodeType", ": #selectNodeType")
    .end()
      .waitForElement("#selectNodeType option")
    . assert.chain()
      .numberOfElements("#selectNodeType option", 7, "7 node types as options")
      .exists("#addNodeBtn", ": #addNodeBtn")
      .exists("#diagramContainer", ": #diagramContainer")
    .end()
    .done();

'Add a node (Indicator)': (test) ->
  test
    .click("#selectNodeType option[value='Indicator']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.Indicator", "Indicator Node")
    .done()

'Add a node (Order)': (test) ->
  test
    .click("#selectNodeType option[value='Order']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.Order", "Order Node")
    .done()

'Add a node (SignalFilter)': (test) ->
  test
    .click("#selectNodeType option[value='SignalFilter']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.SignalFilter", "SignalFilter Node")
    .done()

'Add a node (SignalCombine)': (test) ->
  test
    .click("#selectNodeType option[value='SignalCombine']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.SignalCombine", "SignalCombine Node")
    .done()

'Add a node (SignalThreshold)': (test) ->
  test
    .click("#selectNodeType option[value='SignalThreshold']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.SignalThreshold", "SignalThreshold Node")
    .done()

'Add a node (SignalThreshold)': (test) ->
  test
    .click("#selectNodeType option[value='SignalThreshold']")
    .click("#addNodeBtn")
    .assert.exists("#diagramContainer .node.SignalThreshold", "SignalThreshold Node")
    .done()
