## Client Only Collections
@localNodes = new Mongo.Collection null
@localEdges = new Mongo.Collection null

Session.set "editingNode", null
Session.set "diagram", {}

Session.set "connectionModeOn", false

Session.set "selectedSrc", null
Session.set "selectedDest", null

#

Template.ruleMaker.onCreated ->
  @jsPlumbReady = new ReactiveVar false
  @objectsFromFactory = Meteor.subscribe "objectsFromFactory"
  # Tracker.autorun =>
  #   if @objectsFromFactory.ready()
  #     # console.log 'sub ready'
  #     objectFactory.find({}, {_id:0}).forEach (objProto) ->
  #       delete objProto._id
  #       localNodes.insert objProto
  #
  #     # localNodes.insert {type: "Indicator", ipPortsAvailable: 0, opPortsAvailable: 1, selectable: true, top: 0, left: 100}
  #     # localNodes.insert {type: "Signal", ipPortsAvailable: 1, opPortsAvailable: 1, selectable: true, top: 100, left: 100}
  #     # localNodes.insert {type: "Order", ipPortsAvailable: 1, opPortsAvailable: 0, selectable: true, top: 200, left: 100}

Template.ruleMaker.onRendered ->
  jsPlumb.ready =>
    @jsPlumbReady.set true
    jsPlumb.setContainer "diagramContainer"
    # jsPlumb.draggable ["block1","block2","block3"], {containment: true}
    @common = ## Common options for connections
      endpoint:"Blank"
      anchor: "AutoDefault"
      connector:"Flowchart"
      paintStyle:{ strokeStyle:"black", lineWidth:2 }
      overlays:[ ["PlainArrow", {location: 1}] ]


  Tracker.autorun =>
    # console.log "autorunning"
    if @jsPlumbReady.get() ## attempt to eradicate occassional "tracker afterFlush errors"

      jsPlumb.reset()

      localEdges.find({}).forEach (edge) =>
        # console.log edge.src, edge.dest
        c = jsPlumb.connect {source: edge.src, target: edge.dest}, @common
        ## Bind dblclick to remove connection
        c.bind "dblclick", (conn, evt) =>
          evt.stopPropagation()
          evt.preventDefault()
          if Session.get "connectionModeOn"
            # jsPlumb.detach conn <-- not req, since plumbing is auto-updated on collection change, which is done below
            localEdges.remove({_id: edge._id})
            ## re-increment ports available
            localNodes.update { _id: edge.src }, { $inc: { opPortsAvailable: 1 } }
            localNodes.update { _id: edge.dest }, { $inc: { ipPortsAvailable: 1 } }
          else
            return false

      localNodes.find({}).forEach (node)->
        jsPlumb.draggable(node._id, {containment: true})



Template.ruleMaker.helpers
  addableNodes: ->
    objectFactory.find({}, {fields: {"type": 1}, sort: {type: 1}})
    # return lst
    # for obj in lst
    #   if obj.type[0..5] is "Signal"
    #     obj.typeLabel = "Signal: " + obj.type[6..]
    #   else
    #     obj.typeLabel = obj.type
    # return lst

  labelType: (type) ->
    if type[0..5] is "Signal"
      "Signal: " + type[6..]
    else
      type

  welcomeMessage: ->
    unless localNodes.find({}).count()
      "To Begin, Add Some Nodes from Above!"
    else
      ""

  nodes: ->
    return localNodes.find({})

  editingNode: ->
    localNodes.findOne(_id: Session.get("editingNode"))

  connectionModeOn: ->
    Session.get "connectionModeOn"
  selectedSrc: ->
    Session.get "selectedSrc"
  selectedDest: ->
    Session.get "selectedDest"


  unselectable: ->
    # @._id in Session.get "unselectables"
    if Session.get "connectionModeOn"
      ## to select src
      unless Session.get("selectedSrc")?
        not localNodes.findOne(_id: @._id)?.opPortsAvailable
      else
        ## src selected, now to select dest
        unless Session.get("selectedDest")?
          return true if Session.equals "selectedSrc", @._id  ## src cannot be same as dest
          return true unless localNodes.findOne(_id: @._id)?.ipPortsAvailable
          ## src and dest are already connected!
          return true if localEdges.find({src: Session.get("selectedSrc"), dest: @._id}).count()

          ## Type specific checkings
          srcType = localNodes.findOne(_id: Session.get("selectedSrc"))?.type
          currentType = localNodes.findOne(_id: @._id)?.type
          ## Src node is Signal type
          if srcType[0..5] is "Signal"
            if srcType[6..] isnt "Filter" ## but not signal filter
              ## then dest can either be a signal combine or an order
              return true unless currentType in ["Order", "SignalCombine"]
            else ## Src is a Signal Filter
              ## then dest cannot be Order
              return true unless currentType is "SignalCombine"
          ## Src node is Indicator type
          if srcType is "Indicator"
            return true unless currentType in ["SignalCrossover", "SignalFilter", "SignalThreshold"]
          ## Src node is Price type
          if srcType is "Price"
            return true unless currentType in ["SignalCrossover", "SignalFilter", "SignalThreshold", "Indicator"]

          ## Src node is

  isSrcNode: ->
    Session.equals "selectedSrc", @._id

  isActiveForEditing: ->
    Session.equals "editingNode", @._id


Template.ruleMaker.events
  "change #selectNodeType": (e, t) ->
    e.stopPropagation()
    e.preventDefault()
    $("#addNodeBtn").data("node-type", e.target.value)

  "click #addNodeBtn": (e, t) ->
    e.stopPropagation()
    e.preventDefault()
    type = $("#addNodeBtn").data("node-type") or "Indicator"
    newNode = objectFactory.findOne({type: type}, {fields: {_id: 0}})
    delete newNode._id

    localNodes.insert newNode


  "click #connectionModeOn": (e, t) ->
    e.stopPropagation()
    Session.set "editingNode", null
    Session.set "selectedSrc", null
    Session.set "selectedDest", null
    Session.set "connectionModeOn", not Session.get "connectionModeOn"

  "dblclick #diagramContainer": (e, t) ->
    e.stopPropagation()


  ## Editing (properties of) Node
  "dblclick .node": (e, t) ->
    e.stopPropagation()
    if Session.equals "connectionModeOn", false
      Session.set "editingNode", @._id

  "click #diagramContainer": (e, t) ->  ## clear selection (for editing props)
    # e.stopPropagation()
    if Session.get("editingNode")? and Session.equals("connectionModeOn", false)
      Session.set "editingNode", null

  "submit #editingNodeForm": (e, t) ->
    e.preventDefault()
    for el in $("#editingNodeForm").serializeArray()
      console.log el.name, el.value

  ## Connection mode On
  "click .node": (e, t) ->
    if Session.get "connectionModeOn"
      e.stopPropagation()
       ## click again on selected src to deselect (toggle) it
      if Session.equals "selectedSrc", @._id
        # $(e.target).removeClass("activeAsSrc")
        # $(e.target).removeClass("unselectable")
        Session.set "selectedSrc", null
        Session.set "selectedDest", null
      else
      # Meteor.defer =>
        unselectable = $("#"+@._id).hasClass("unselectable")
        # unselectable = $(e.target).hasClass("unselectable")
        unless Session.get("selectedSrc")? ## this click is menat to select source node
          Session.set("selectedSrc", @._id) if not unselectable
        else    ## src already chosen, this click is meant to select dest
          if not unselectable
            Session.set "selectedDest", @._id
            sourceID = Session.get "selectedSrc"
            targetID = Session.get "selectedDest"
            ## reset session variables
            Session.set "connectionModeOn", false
            Session.set "selectedSrc", null
            Session.set "selectedDest", null
            ## Now make an edge
            edge = {src: sourceID, dest: targetID}
            # Meteor.defer ->
            if localEdges.find(edge).count() is 0 ## avoid entering same connection/edge
              localEdges.insert edge ## connection!
              ## Decrement available ports
              localNodes.update { _id: sourceID }, { $inc: { opPortsAvailable: -1 } }
              localNodes.update { _id: targetID }, { $inc: { ipPortsAvailable: -1 } }
